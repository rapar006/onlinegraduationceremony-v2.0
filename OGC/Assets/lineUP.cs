﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class lineUP : MonoBehaviour
{
    public GameObject[] students;
    public GameObject nextCube;
    public Animator animator;
    public Transform myTransform;
    Vector3 myPosition;
    public VideoPlayer speech_1_video;
    bool areWeDone = true;
    private float time = 0f;
    public float timeClapping = 1f;
    int i = 0;
    public int waitTime = 5;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(speech_1_video.isPlaying == false)
        {
            //Students start clapping
           VideoHasEnded(students);



            time += 0.01f;
            //after clapping students start walking one by one
            if (time > timeClapping && areWeDone)
            {
               
                   students[i].GetComponent<Animator>().SetTrigger("StopClapping");
                    Vector3 targetPosition;
                    targetPosition = new Vector3(transform.position.x - students[i].transform.position.x, 0f, transform.position.z - students[i].transform.position.z);
                    Quaternion rotation = Quaternion.LookRotation(targetPosition);
                    students[i].transform.rotation = Quaternion.Slerp(students[i].transform.rotation, rotation, 0.05f);
                    students[i].transform.Translate(Vector3.forward * 0.05f);
                  if (Vector3.Distance(students[i].transform.position, this.transform.position) < 1.1)
                  {


                    students[i].transform.rotation = this.transform.rotation;
                    nextCube.GetComponent<cube1>().enabled = true;
                    if (i < students.Length)
                    {
                        i++;
                    }
                    else
                    {
                        areWeDone = false;
                    }

                  }
            }
                
        

        }
                
               
        }
  
    

    public void waitForVideoEnd()
    {
        while(speech_1_video.isPlaying == true)
        {
            //do nothing
        }
        return;
    }

    public int MoveAllStudents()
    {

        for (int i = 0; i < students.Length; i++)
        {
            Debug.Log("student"+i+ "should start clapping");
            MoveAStudent(students[i], nextCube, "cornerPoint");
            Debug.Log("wait 5 seconds for next");
            Wait5();
            Debug.Log("After 5 second next loop");
        }

        return 0;
    }

    IEnumerator Wait5()
    {
        yield return new WaitForSeconds(waitTime);
    }

    public void VideoHasEnded(GameObject[] array)
    {
        for(int i=0; i<array.Length; i++)
        {
            array[i].GetComponent<Animator>().SetTrigger("SpeechHasEnded");
        }
    }

    public void MoveAStudent(GameObject student, GameObject aCube, string cubeScript)
    {
        student.GetComponent<Animator>().SetTrigger("StopClapping");
        Vector3 targetPosition;
        targetPosition = new Vector3(transform.position.x - student.transform.position.x, 0f, transform.position.z - student.transform.position.z);
        Quaternion rotation = Quaternion.LookRotation(targetPosition);
        student.transform.rotation = Quaternion.Slerp(student.transform.rotation, rotation, 0.05f);
        student.transform.Translate(Vector3.forward * 0.05f);

        if (Vector3.Distance(student.transform.position, this.transform.position) < 1.1)
        {


            student.transform.rotation = this.transform.rotation;
            aCube.GetComponent<cubeScript>().enabled = true;
            //student = null;
            //areWeDone = false;
            //  aStudent[i].GetComponent<Animator>().SetTrigger("StartIdle");

        }
    }
}
