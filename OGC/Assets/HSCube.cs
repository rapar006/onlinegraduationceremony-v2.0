﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HSCube : MonoBehaviour
{
    public GameObject[] students;
    public GameObject nextCube, president;
    int i = 0;
    GameObject temp;
    private int waitTime = 18;
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

       
            Vector3 targetPosition;
            targetPosition = new Vector3(transform.position.x - students[i].transform.position.x, 0f, transform.position.z - students[i].transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            students[i].transform.rotation = Quaternion.Slerp(students[i].transform.rotation, rotation, 0.05f);
            students[i].transform.Translate(Vector3.forward * 0.05f);
            
            if (Vector3.Distance(students[i].transform.position, this.transform.position) < 1)
            {
                students[i].GetComponent<Animator>().SetTrigger("ShakeHands");
           
                president.GetComponent<Animator>().SetTrigger("ShakeHands");
            temp = students[i];
            students[i] = null;
            StartCoroutine(corutine());
            StartCoroutine(corutine2());
            
            
            //nextCube.GetComponent<cubeScript5>().enabled = true;
          // StartCoroutine(corutine2());
                //areWeDone = false;
               // if (!anim.IsPlaying("ShakingHands2"));
           
                
                //upCube.GetComponent<upRampFinal>().enabled = true;

            }
        
     /*  if(areWeDone ==false && timer > timerEnd)
        {
            animator.SetTrigger("WalkNow");
            nextCube.GetComponent<cubeScript5>().enabled = true;

        }*/

     }

    IEnumerator corutine()
    {

        yield return new WaitForSeconds(5);
        temp.GetComponent<Animator>().SetTrigger("WalkNow");
        nextCube.GetComponent<cubeScript5>().enabled = true;



    }
    IEnumerator corutine2()
    {
        
        yield return new WaitForSeconds(waitTime);
        
        i++;


    }
}
