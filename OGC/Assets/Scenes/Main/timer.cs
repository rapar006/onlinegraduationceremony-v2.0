﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class timer : MonoBehaviour
{
    //public string nextScene;
    private Text timerText;
    public float myTimer = 10f;
    public Animator anim;
    private bool check = false;
    public Image blackImage;
    // Start is called before the first frame update
    void Start()
    {
        timerText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        myTimer -= Time.deltaTime;
        timerText.text = myTimer.ToString("f0");
        if(myTimer <=0 && check == false)
        {
           
            anim.SetTrigger("nextScene");
            check = true;

           // blackImage.GetComponent<Image>().enabled = true;

            //SceneManager.LoadScene(2);
        }
    }

    void OnFadeComplete()
    {
        SceneManager.LoadScene(2);
    }

   
}
