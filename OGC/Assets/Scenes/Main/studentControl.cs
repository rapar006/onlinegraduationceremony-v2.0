﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class studentControl : MonoBehaviour
{
    //code based on Holistic3d
    static Animator animator;
    public float speed = 1.0F;
    public float rotationSpeed = 100.0F;

   
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
      
    }

    // Update is called once per frame
    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);


        if(translation !=0)
        {
            animator.SetBool("isWalk", true);
        }
        else
        {
            animator.SetBool("isWalk", false);
        }

     
    }
}
