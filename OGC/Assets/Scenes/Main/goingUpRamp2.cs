﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goingUpRamp2 : MonoBehaviour
{
    //Our student object
    public GameObject aStudent;

    //component from student object
    Animator studentAnimator;

    //this will be true when user wants to go up ramp 
    bool goUpRamp = false;

    //unity method that is called the user clicks
    //calls trigger isWalking from student animator controller
 
    private void OnMouseDown()
    {
        studentAnimator.SetTrigger("isWalking2");
        goUpRamp = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Gets the animator from our student object
        studentAnimator = aStudent.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(goUpRamp)
        {
            //moving and rotation character with chair
            Vector3 targetPosition;
            targetPosition = new Vector3(transform.position.x - aStudent.transform.position.x, transform.position.y - aStudent.transform.position.y, transform.position.z - aStudent.transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            aStudent.transform.rotation = Quaternion.Slerp(aStudent.transform.rotation, rotation, 0.05f);
            aStudent.transform.Translate(Vector3.forward * 0.01f);
            //when distance between chair and character less than 0.8 isSitting will trigger
            if(Vector3.Distance(aStudent.transform.position, this.transform.position) < 0.6)
            {
                studentAnimator.SetTrigger("isGoingUp");
                aStudent.transform.rotation = this.transform.rotation;

                goUpRamp = false;
            }
        }
    }
}
