﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CameraMovement : MonoBehaviour
{

    public GameObject aCamera;
    public VideoPlayer myVideo;
    public Transform myTransform;
    public Animator animator;

    bool areWeDone = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (myVideo.isPlaying == false && !areWeDone)
        {
            Vector3 targetPosition;
            targetPosition = new Vector3(transform.position.x - aCamera.transform.position.x, 0f, transform.position.z - aCamera.transform.position.z);
            aCamera.transform.Translate(Vector3.back * 0.05f);
            if (Vector3.Distance(aCamera.transform.position, this.transform.position) < 0.9)
            {
                areWeDone = true;
            }


        }
    }
}
