﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speech2 : MonoBehaviour
{
    public Animator animator;
    public GameObject aStudent;
    Vector3 targetPosition;
    bool areWeDone = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (areWeDone)
        {
            animator.SetTrigger("WalkNow");
            targetPosition = new Vector3(transform.position.x - aStudent.transform.position.x, 0f, transform.position.z - aStudent.transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(targetPosition);
            aStudent.transform.rotation = Quaternion.Slerp(aStudent.transform.rotation, rotation, 0.05f);
            aStudent.transform.Translate(Vector3.forward * 0.05f);
            //when distance between chair and character less than 0.8 isSitting will trigger
            if (Vector3.Distance(aStudent.transform.position, this.transform.position) < 1)
            {

                animator.SetTrigger("Clapping");

                areWeDone = false;

            }
        }
    }
}
