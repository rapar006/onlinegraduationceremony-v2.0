﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handShake : MonoBehaviour
{

    public Animator animator;
    bool areWeDone = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (areWeDone)
        {
            animator.SetTrigger("ShakeHands");
            areWeDone = false;
            //this.GetComponent<handShake>().enabled = false;
        }
    }
}
